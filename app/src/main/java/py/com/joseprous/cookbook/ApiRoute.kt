package py.com.joseprous.cookbook

import android.content.Context
import com.android.volley.*

sealed class ApiRoute {

    val timeOut: Int
        get() {
            return 3000
        }

    private val baseUrl: String
        get() {
            return MAIN_URL
        }

    data class GetRecipes(var ctx: Context) : ApiRoute()
    data class GetRecipe(var ctx: Context, var id: Int) : ApiRoute()

    val httpMethod: Int
        get() {
            return when (this) {
                is GetRecipes -> Request.Method.GET
                is GetRecipe -> Request.Method.GET
            }
        }

    val url: String
        get() {
            return "$baseUrl/${when (this@ApiRoute) {
                is GetRecipes -> "recipes"
                is GetRecipe -> "recipes/$id"
            }}"
        }

}