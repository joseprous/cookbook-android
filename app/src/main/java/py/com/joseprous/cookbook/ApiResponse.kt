package py.com.joseprous.cookbook

import org.json.JSONArray
import org.json.JSONObject
import org.json.JSONTokener

class ApiResponse(response: String) {

    var json: String = ""

    init {
        try {
            val jsonToken = JSONTokener(response).nextValue()
            if (jsonToken is JSONObject) {
                val jsonResponse = JSONObject(response)
                json = jsonResponse.toString()
            }
            if (jsonToken is JSONArray) {
                val jsonResponse = JSONArray(response)
                json = jsonResponse.toString()
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

}