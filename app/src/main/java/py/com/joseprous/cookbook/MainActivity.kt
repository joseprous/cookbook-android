package py.com.joseprous.cookbook

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.WindowManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

class MainActivity : AppCompatActivity() {
    private lateinit var recyclerView: RecyclerView
    private lateinit var viewAdapter: RecyclerView.Adapter<*>
    private lateinit var viewManager: RecyclerView.LayoutManager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        window.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)

        NukeSSLCerts.nuke()

        ApiClient(this).getRecipes { recipes, _ ->
            viewManager = LinearLayoutManager(this)
            viewAdapter = RecipesAdapter(recipes, this)

            recyclerView = findViewById<RecyclerView>(R.id.main_recycler_view).apply {
                layoutManager = viewManager
                adapter = viewAdapter
            }
        }
    }
}
