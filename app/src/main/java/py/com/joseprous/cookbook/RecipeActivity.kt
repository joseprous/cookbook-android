package py.com.joseprous.cookbook

import android.os.Bundle
import android.view.WindowManager
import android.widget.LinearLayout
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.RecyclerView


class RecipeActivity : AppCompatActivity() {
    private lateinit var linearLayout: LinearLayout
    private lateinit var viewAdapter: RecyclerView.Adapter<*>
    private lateinit var viewManager: RecyclerView.LayoutManager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.recipe)
        window.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)

        val id = intent.getIntExtra("id", 0)
        ApiClient(this).getRecipe(id) { recipe, _ ->
            if(recipe != null) {
                val text = findViewById<TextView>(R.id.text)
                val name = findViewById<TextView>(R.id.name)
                name.text = recipe.name
                text.text = recipe.text
            }
        }
    }
}