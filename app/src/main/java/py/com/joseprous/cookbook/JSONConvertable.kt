package py.com.joseprous.cookbook

import com.google.gson.Gson
import com.google.gson.annotations.SerializedName
import org.json.JSONArray

interface JSONConvertable {
    fun toJSON(): String = Gson().toJson(this)
}

inline fun <reified T: JSONConvertable> String.toObject(): T = Gson().fromJson(this, T::class.java)

data class Recipe(
    @SerializedName("id") val id: Int,
    @SerializedName("name") val name: String,
    @SerializedName("text") val text: String) : JSONConvertable

inline fun <reified T : JSONConvertable> JSONArray.toArrayList(): ArrayList<T> {
    val array: ArrayList<T> = arrayListOf()
    for (i in 0 until this.length()) {
        array.add(this.getJSONObject(i).toString().toObject())
    }
    return array
}